using System;
using System.Runtime.InteropServices;

#if WINDOWS

namespace Ntx.GD
{
    /// <summary>
    /// GD APIs
    /// </summary>
	public class GDImport
	{
		private const string LIBC = "msvcrt.dll";
		private const string LIBGD = "bgd.dll";

		#region GD API declarations

		[DllImport(LIBGD, EntryPoint = "#6", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdFontGetGiant();

		[DllImport(LIBGD, EntryPoint = "#7", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdFontGetLarge();

		[DllImport(LIBGD, EntryPoint = "#8", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdFontGetMediumBold();

		[DllImport(LIBGD, EntryPoint = "#9", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdFontGetSmall();

		[DllImport(LIBGD, EntryPoint = "#10", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdFontGetTiny();


		[DllImport(LIBGD, EntryPoint = "#16", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdFree(IntPtr handle);

		[DllImport(LIBGD, EntryPoint = "#19", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageAlphaBlending(IntPtr handle, int blending);

		[DllImport(LIBGD, EntryPoint = "#20", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageArc(IntPtr handle, int cx, int cy, int w, int h, int s, int e, int color);

		[DllImport(LIBGD, EntryPoint = "#21", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageBoundsSafe(IntPtr handle, int x, int y);

		[DllImport(LIBGD, EntryPoint = "#22", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageChar(IntPtr handle, IntPtr fontHandle, int x, int y, int c, int color);

		[DllImport(LIBGD, EntryPoint = "#23", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageCharUp(IntPtr handle, IntPtr fontHandle, int x, int y, int c, int color);

		[DllImport(LIBGD, EntryPoint = "#24", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageColorAllocate(IntPtr handle, int r, int g, int b);

		[DllImport(LIBGD, EntryPoint = "#25", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageColorAllocateAlpha(IntPtr handle, int r, int g, int b, int a);

		[DllImport(LIBGD, EntryPoint = "#26", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageColorClosest(IntPtr handle, int r, int g, int b);


		[DllImport(LIBGD, EntryPoint = "#27", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageColorClosestAlpha(IntPtr handle, int r, int g, int b, int a);

		[DllImport(LIBGD, EntryPoint = "#28", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageColorClosestHWB(IntPtr handle, int r, int g, int b);

		[DllImport(LIBGD, EntryPoint = "#29", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageColorDeallocate(IntPtr handle, int color);

		[DllImport(LIBGD, EntryPoint = "#30", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageColorExact(IntPtr handle, int r, int g, int b);


		[DllImport(LIBGD, EntryPoint = "#32", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageColorResolve(IntPtr handle, int r, int g, int b);

		[DllImport(LIBGD, EntryPoint = "#33", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageColorResolveAlpha(IntPtr handle, int r, int g, int b, int a);

		[DllImport(LIBGD, EntryPoint = "#34", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageColorTransparent(IntPtr handle, int color);

		[DllImport(LIBGD, EntryPoint = "#35", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageCompare(IntPtr handle1, IntPtr handle2);

		[DllImport(LIBGD, EntryPoint = "#36", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageCopy(IntPtr dst, IntPtr src, int dstX, int dstY, int srcX, int srcY, int w, int h);

		[DllImport(LIBGD, EntryPoint = "#37", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageCopyMerge(IntPtr dst, IntPtr src, int dstX, int dstY, int srcX, int srcY, int w, int h, int pct);


		[DllImport(LIBGD, EntryPoint = "#39", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageCopyResampled(IntPtr dst, IntPtr src, int dstX, int dstY, int srcX, int srcY, int destW, int destH, int srcW, int srcH);

		[DllImport(LIBGD, EntryPoint = "#40", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageCopyResized(IntPtr dst, IntPtr src, int dstX, int dstY, int srcX, int srcY, int destW, int destH, int srcW, int srcH);

		[DllImport(LIBGD, EntryPoint = "#41", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageCopyRotated(IntPtr dst, IntPtr src, double dstX, double dstY, int srcX, int srcY, int srcW, int srcH, int angle);

		[DllImport(LIBGD, EntryPoint = "#42", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreate(int width, int height);

		[DllImport(LIBGD, EntryPoint = "#43", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromGd2(IntPtr fileHandle);


		[DllImport(LIBGD, EntryPoint = "#48", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromGd2Ptr(int size, byte[] data);

		[DllImport(LIBGD, EntryPoint = "#49", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromGd(IntPtr fileHandle);


		[DllImport(LIBGD, EntryPoint = "#51", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromGdPtr(int size, byte[] data);

		[DllImport(LIBGD, EntryPoint = "#52", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromGif(IntPtr fileHandle);


		[DllImport(LIBGD, EntryPoint = "#54", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromGifPtr(int size, byte[] data);

		[DllImport(LIBGD, EntryPoint = "#55", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromJpeg(IntPtr fileHandle);


		[DllImport(LIBGD, EntryPoint = "#57", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromJpegPtr(int size, byte[] data);


		[DllImport(LIBGD, EntryPoint = "#58", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromPng(IntPtr fileHandle);

		[DllImport(LIBGD, EntryPoint = "#60", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromPngPtr(int size, IntPtr handle);

		[DllImport(LIBGD, EntryPoint = "#60", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromPngPtr(int size, byte[] data);

		[DllImport(LIBGD, EntryPoint = "#62", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromWBMP(IntPtr fileHandle);


		[DllImport(LIBGD, EntryPoint = "#64", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromWBMPPtr(int size, byte[] data);

		[DllImport(LIBGD, EntryPoint = "#65", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromXbm(IntPtr fileHandle);

		[DllImport(LIBGD, EntryPoint = "#66", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateFromXpm(IntPtr fileHandle);


		[DllImport(LIBGD, EntryPoint = "#68", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageCreateTrueColor(int width, int height);

		[DllImport(LIBGD, EntryPoint = "#69", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageDashedLine(IntPtr handle, int x1, int y1, int x2, int y2, int color);

		[DllImport(LIBGD, EntryPoint = "#70", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageDestroy(IntPtr handle);

		[DllImport(LIBGD, EntryPoint = "#71", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageFill(IntPtr handle, int x, int y, int color);

		[DllImport(LIBGD, EntryPoint = "#72", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageFillToBorder(IntPtr handle, int x, int y, int border, int color);

		[DllImport(LIBGD, EntryPoint = "#73", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageFilledArc(IntPtr handle, int cx, int cy, int w, int h, int s, int e, int color, int style);

		[DllImport(LIBGD, EntryPoint = "#74", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageFilledEllipse(IntPtr handle, int cx, int cy, int w, int h, int color);

		[DllImport(LIBGD, EntryPoint = "#75", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageFilledPolygon(IntPtr handle, int[] points, int pointsTotal, int color);

		[DllImport(LIBGD, EntryPoint = "#76", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageFilledRectangle(IntPtr handle, int x1, int y1, int x2, int y2, int color);

		[DllImport(LIBGD, EntryPoint = "#77", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageGd2(IntPtr handle, IntPtr fileHandle);

		[DllImport(LIBGD, EntryPoint = "#78", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageGd2Ptr(IntPtr handle, int chunkSize, int fmt, out int size);

		[DllImport(LIBGD, EntryPoint = "#79", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageGd(IntPtr handle, IntPtr fileHandle);

		[DllImport(LIBGD, EntryPoint = "#80", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageGdPtr(IntPtr handle, out int size);

		[DllImport(LIBGD, EntryPoint = "#81", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageGetClip(IntPtr handle, ref int x1, ref int y1, ref int x2, ref int y2);

		[DllImport(LIBGD, EntryPoint = "#82", CallingConvention = CallingConvention.Cdecl)]
		public static extern int gdImageGetPixel(IntPtr handle, int x, int y);


		[DllImport(LIBGD, EntryPoint = "#84", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageGif(IntPtr handle, IntPtr fileHandle);


		[DllImport(LIBGD, EntryPoint = "#87", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageGifAnimAddPtr(IntPtr handle, out int size, int LocalCM, int LeftOfs, int TopOfs, int Delay, int Disposal, IntPtr previm);


		[DllImport(LIBGD, EntryPoint = "#90", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageGifAnimBeginPtr(IntPtr handle, out int size, int GlobalCM, int Loops);


		[DllImport(LIBGD, EntryPoint = "#93", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageGifAnimEndPtr(out int size);

		[DllImport(LIBGD, EntryPoint = "#96", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageInterlace(IntPtr handle, int interlace);

		[DllImport(LIBGD, EntryPoint = "#97", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageJpeg(IntPtr handle, IntPtr fileHandle, int quality);


		[DllImport(LIBGD, EntryPoint = "#99", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageJpegPtr(IntPtr handle, out int size, int quality);

		[DllImport(LIBGD, EntryPoint = "#100", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageLine(IntPtr handle, int x1, int y1, int x2, int y2, int color);


		[DllImport(LIBGD, EntryPoint = "#102", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImagePaletteCopy(IntPtr dst, IntPtr src);

		[DllImport(LIBGD, EntryPoint = "#103", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImagePng(IntPtr handle, IntPtr fileHandle);


		[DllImport(LIBGD, EntryPoint = "#106", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImagePngEx(IntPtr handle, IntPtr fileHandle, int level);

		[DllImport(LIBGD, EntryPoint = "#107", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImagePngPtr(IntPtr handle, out int size);


		[DllImport(LIBGD, EntryPoint = "#110", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImagePolygon(IntPtr handle, int[] points, int pointsTotal, int color);

		[DllImport(LIBGD, EntryPoint = "#111", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageRectangle(IntPtr handle, int x1, int y1, int x2, int y2, int color);

		[DllImport(LIBGD, EntryPoint = "#112", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSaveAlpha(IntPtr handle, int saveFlag);

		[DllImport(LIBGD, EntryPoint = "#113", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSetAntiAliased(IntPtr handle, int c);

		[DllImport(LIBGD, EntryPoint = "#114", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSetAntiAliasedDontBlend(IntPtr handle, int c);

		[DllImport(LIBGD, EntryPoint = "#115", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSetBrush(IntPtr handle, IntPtr brushHandle);

		[DllImport(LIBGD, EntryPoint = "#116", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSetClip(IntPtr handle, int x1, int y1, int x2, int y2);

		[DllImport(LIBGD, EntryPoint = "#117", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSetPixel(IntPtr handle, int x, int y, int color);

		[DllImport(LIBGD, EntryPoint = "#118", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSetStyle(IntPtr handle, int[] style, int styleLength);

		[DllImport(LIBGD, EntryPoint = "#119", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSetThickness(IntPtr handle, int thickness);

		[DllImport(LIBGD, EntryPoint = "#120", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSetTile(IntPtr handle, IntPtr tileHandle);

		[DllImport(LIBGD, EntryPoint = "#121", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSharpen(IntPtr handle, int pct);

		[DllImport(LIBGD, EntryPoint = "#122", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageSquareToCircle(IntPtr handle, int radius);

		[DllImport(LIBGD, EntryPoint = "#124", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageString(IntPtr handle, IntPtr fontHandle, int x, int y, string message, int color);

		[DllImport(LIBGD, EntryPoint = "#125", CallingConvention = CallingConvention.Cdecl)]
		public static extern string gdImageStringFT(IntPtr handle, int[] brect, int fg, string fontname, double ptsize, double angle, int x, int y, string message);

		[DllImport(LIBGD, EntryPoint = "#126", CallingConvention = CallingConvention.Cdecl)]
		public static extern string gdImageStringFTCircle(IntPtr handle, int cx, int cy, double radius, double textRadius, double fillPortion, string font, double points, string top, string bottom, int fgcolor);


		[DllImport(LIBGD, EntryPoint = "#130", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageStringUp(IntPtr handle, IntPtr fontHandle, int x, int y, string message, int color);

		[DllImport(LIBGD, EntryPoint = "#131", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageTrueColorToPalette(IntPtr handle, int ditherFlag, int colorsWanted);

		[DllImport(LIBGD, EntryPoint = "#132", CallingConvention = CallingConvention.Cdecl)]
		public static extern void gdImageWBMP(IntPtr handle, int fg, IntPtr fileHandle);

		[DllImport(LIBGD, EntryPoint = "#134", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr gdImageWBMPPtr(IntPtr handle, out int size);

#endregion
		
#region File Handling

		[DllImport(LIBC)]
		public static extern IntPtr fopen(string filename, string mode);

		[DllImport(LIBC)]
		public static extern void fclose(IntPtr file);

#endregion

	}

}

#endif
