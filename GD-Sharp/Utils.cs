﻿using System;
using System.Text;

namespace Ntx.GD
{
	internal static class Utils
    {
		public static Encoding GetDefaultEncoding()
		{
#if CORECLR
			return Encoding.UTF8;
#else
			return Encoding.Default;
#endif
		}
    }
}
