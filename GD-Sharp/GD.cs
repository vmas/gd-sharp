/*************************************************************************************************************************\

Author:		Mircea-Cristian Racasan <darx_kies@gmx.net>
            Chris Turchin <chris@turchin.net>
			Kevin Tam <kevin@glorat.net>
Copyright: 	2005 by Mircea-Cristian Racasan
            Portions by Kevin Tam 2005

This program is free software; you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; 
if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

\*************************************************************************************************************************/

using System;
using System.IO;
using System.Collections;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;
using System.Reflection;

namespace Ntx.GD
{
	public class GD : IDisposable
	{
		#region GD Constants
		/***********************************************************************************\
		GD Constants
		\***********************************************************************************/
		public const int GD_MAX_COLORS = 256;
		public static GDColor GD_ANTIALIASED = new GDColor(-7);
		public const int GD_BRUSHED = -3;
		public const int GD_TILED = -5;
		public static GDColor GD_STYLED_BRUSHED = new GDColor(-4);
		public const int GD_STYLED = -2;
		public const int GD_DASH_SIZE = 4;
		public const int GD_TRANSPARENT = -7;
		// for saving GD2 format
		private const int GD2_FMT_COMPRESSED = 2;








		[StructLayout(LayoutKind.Sequential)]
		private struct GDImage
		{
			public IntPtr pixels;
			public int sx;
			public int sy;
			public int colorsTotal;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = GD_MAX_COLORS)]
			public int[] red;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = GD_MAX_COLORS)]
			public int[] green;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = GD_MAX_COLORS)]
			public int[] blue;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = GD_MAX_COLORS)]
			public int[] open;
			public int transparent;
			public IntPtr polyInts;
			public int polyAllocated;
			public IntPtr brush;
			public IntPtr tile;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = GD_MAX_COLORS)]
			public int[] brushColorMap;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = GD_MAX_COLORS)]
			public int[] tileColorMap;
			public int styleLength;
			public int stylePos;
			public IntPtr style;
			public int interlace;
			public int thick;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = GD_MAX_COLORS)]
			public int[] alpha;
			public int trueColor;
			public IntPtr tpixels;
			public int alphaBlendingFlag;
			public int saveAlphaFlag;



			private static Dictionary<string, int> mFieldOffsets;
			static GDImage()
			{
				// Cache field offsets for faster lookup
				mFieldOffsets = new Dictionary<string, int>();
				Type t = typeof(GDImage);
#if CORECLR
				foreach (FieldInfo field in t.GetTypeInfo().GetFields())
				{
					mFieldOffsets[field.Name] = Marshal.OffsetOf<GDImage>(field.Name).ToInt32();
				}
#else
				foreach (FieldInfo field in t.GetFields())
				{
					mFieldOffsets[field.Name] = Marshal.OffsetOf(t, field.Name).ToInt32();
				}
#endif
			}

			/// <summary>
			/// Returns the offset in bytes from the start of the GDImage structure
			/// to the given field
			/// </summary>
			/// <param name="field">Field name</param>
			/// <returns>Offset in bytes</returns>
			public static int GetOffset(string field)
			{
				return mFieldOffsets[field];
			}
		}

		#endregion

		#region Class member variables
		private IntPtr handle;
		private bool disposed = false;
		#endregion

		#region Color Methods
		/***********************************************************************************\
		GD Color
		\***********************************************************************************/

		public GDColor ColorAllocate(int r, int g, int b)
		{
			GDColor color = (GDColor)GDImport.gdImageColorAllocate(this.Handle, r, g, b);
			GC.KeepAlive(this);
			return color;
		}

		public GDColor ColorAllocateAlpha(int r, int g, int b, int a)
		{
			GDColor color = (GDColor)GDImport.gdImageColorAllocateAlpha(this.Handle, r, g, b, a);
			GC.KeepAlive(this);
			return color;
		}

		public void ColorDeallocate(GDColor color)
		{
			GDImport.gdImageColorDeallocate(this.Handle, color);
			GC.KeepAlive(this);
		}

		public GDColor ColorClosest(int r, int g, int b)
		{
			GDColor color = (GDColor)GDImport.gdImageColorClosest(this.Handle, r, g, b);
			GC.KeepAlive(this);
			return color;
		}

		public GDColor ColorClosestAlpha(int r, int g, int b, int a)
		{
			GDColor color = (GDColor)GDImport.gdImageColorClosestAlpha(this.Handle, r, g, b, a);
			GC.KeepAlive(this);
			return color;
		}

		public GDColor ColorClosestHWB(int r, int g, int b)
		{
			GDColor color = (GDColor)GDImport.gdImageColorClosestHWB(this.Handle, r, g, b);
			GC.KeepAlive(this);
			return color;
		}

		public GDColor ColorExact(int r, int g, int b)
		{
			GDColor color = (GDColor)GDImport.gdImageColorExact(this.Handle, r, g, b);
			GC.KeepAlive(this);
			return color;
		}

		public GDColor ColorResolve(int r, int g, int b)
		{
			GDColor color = (GDColor)GDImport.gdImageColorResolve(this.Handle, r, g, b);
			GC.KeepAlive(this);
			return color;
		}

		public GDColor ColorResolveAlpha(int r, int g, int b, int a)
		{
			GDColor color = (GDColor)GDImport.gdImageColorResolveAlpha(this.Handle, r, g, b, a);
			GC.KeepAlive(this);
			return color;
		}

		public void ColorTransparent(GDColor color)
		{
			GDImport.gdImageColorTransparent(this.Handle, color);
			GC.KeepAlive(this);
		}
		#endregion

		#region Drawing/Styling/Brushing/Tiling/Filling
		/***********************************************************************************\
		GD Drawing/Styling/Brushing/Tiling/Filling
		\***********************************************************************************/

		public void SetPixel(int x, int y, GDColor color)
		{
			GDImport.gdImageSetPixel(this.Handle, x, y, color);
			GC.KeepAlive(this);
		}

		public void Line(int x1, int y1, int x2, int y2, GDColor color)
		{
			GDImport.gdImageLine(this.Handle, x1, y1, x2, y2, color);
			GC.KeepAlive(this);
		}

		public void DashedLine(int x1, int y1, int x2, int y2, GDColor color)
		{
			GDImport.gdImageDashedLine(this.Handle, x1, y1, x2, y2, color);
			GC.KeepAlive(this);
		}


		public void Rectangle(int x1, int y1, int x2, int y2, GDColor color)
		{
			GDImport.gdImageRectangle(this.Handle, x1, y1, x2, y2, color);
			GC.KeepAlive(this);
		}

		public void FilledRectangle(int x1, int y1, int x2, int y2, GDColor color)
		{
			GDImport.gdImageFilledRectangle(this.Handle, x1, y1, x2, y2, color);
			GC.KeepAlive(this);
		}


		public void Polygon(List<Point> list, GDColor color)
		{
			int[] intList = Point.GetIntArray(list);
			GDImport.gdImagePolygon(this.Handle, intList, list.Count, color);
			GC.KeepAlive(this);
		}


		public void FilledPolygon(List<Point> list, GDColor color)
		{
			int[] intList = Point.GetIntArray(list);
			GDImport.gdImageFilledPolygon(this.Handle, intList, list.Count, color);
			GC.KeepAlive(this);
		}


		public void Arc(int cx, int cy, int w, int h, int s, int e, GDColor color)
		{
			GDImport.gdImageArc(this.Handle, cx, cy, w, h, s, e, color);
			GC.KeepAlive(this);
		}


		public void FilledArc(int cx, int cy, int w, int h, int s, int e, GDColor color, int style)
		{
			GDImport.gdImageFilledArc(this.Handle, cx, cy, w, h, s, e, color, style);
			GC.KeepAlive(this);
		}


		public void FilledEllipse(int cx, int cy, int w, int h, GDColor color)
		{
			GDImport.gdImageFilledEllipse(this.Handle, cx, cy, w, h, color);
			GC.KeepAlive(this);
		}

		public void FillToBorder(int x, int y, int border, GDColor color)
		{
			GDImport.gdImageFillToBorder(this.Handle, x, y, border, color);
			GC.KeepAlive(this);
		}

		public void Fill(int x, int y, GDColor color)
		{
			GDImport.gdImageFill(this.Handle, x, y, color);
			GC.KeepAlive(this);
		}

		public void SetAntiAliased(GDColor c)
		{
			GDImport.gdImageSetAntiAliased(this.Handle, c);
			GC.KeepAlive(this);
		}

		public void SetAntiAliasedDontBlend(GDColor c)
		{
			GDImport.gdImageSetAntiAliasedDontBlend(this.Handle, c);
			GC.KeepAlive(this);
		}

		public void SetBrush(GD brush)
		{
			GDImport.gdImageSetBrush(this.Handle, brush.GetHandle());
			GC.KeepAlive(this);
		}


		public void SetTile(GD brush)
		{
			GDImport.gdImageSetTile(this.Handle, brush.GetHandle());
			GC.KeepAlive(this);
		}

		public void SetStyle(int[] style)
		{
			GDImport.gdImageSetStyle(this.Handle, style, style.Length);
			GC.KeepAlive(this);
		}

		public void SetThickness(int thickness)
		{
			GDImport.gdImageSetThickness(this.Handle, thickness);
			GC.KeepAlive(this);
		}

		public bool AlphaBlending
		{
			get
			{
				return IntToBool(GetStructInt("alphaBlendingFlag"));
			}
			set
			{
				GDImport.gdImageAlphaBlending(this.Handle, BoolToInt(value));
				GC.KeepAlive(this);
			}
		}

		public bool SaveAlpha
		{
			get
			{
				return IntToBool(GetStructInt("saveAlphaFlag"));
			}
			set
			{
				GDImport.gdImageSaveAlpha(this.Handle, BoolToInt(value));
				GC.KeepAlive(this);
			}
		}

		public void SetClip(int x1, int y1, int x2, int y2)
		{
			GDImport.gdImageSetClip(this.Handle, x1, y1, x2, y2);
			GC.KeepAlive(this);
		}
		#endregion

		#region Font/Text
		/***********************************************************************************\
		GD Font/Text
		\***********************************************************************************/

		public void Char(Font font, int x, int y, int c, GDColor color)
		{
			IntPtr fontHandle = font.GetHandle();
			GDImport.gdImageChar(this.Handle, fontHandle, x, y, c, color);
			GC.KeepAlive(this);
		}

		public void CharUp(Font font, int x, int y, int c, GDColor color)
		{
			IntPtr fontHandle = font.GetHandle();
			GDImport.gdImageCharUp(this.Handle, fontHandle, x, y, c, color);
			GC.KeepAlive(this);
		}

		public void String(Font font, int x, int y, string message, GDColor color)
		{
			IntPtr fontHandle = font.GetHandle();
			GDImport.gdImageString(this.Handle, fontHandle, x, y, message, color);
			GC.KeepAlive(this);
		}


		public void StringUp(Font font, int x, int y, string message, GDColor color)
		{
			IntPtr fontHandle = font.GetHandle();
			GDImport.gdImageStringUp(this.Handle, fontHandle, x, y, message, color);
			GC.KeepAlive(this);
		}


		public string StringFT(out Rectangle bounds, GDColor color, string fontname, double ptsize, double angle, int x, int y, string message, bool draw)
		{
			int[] brect = new int[8];

			Encoding encoding = Utils.GetDefaultEncoding();
			string result = GDImport.gdImageStringFT(draw ? this.Handle : IntPtr.Zero, brect, color.Index, fontname, ptsize, angle, x, y,
			encoding.GetString(Encoding.Convert(encoding, Encoding.UTF8, encoding.GetBytes(message))));
			GC.KeepAlive(this);

			Debug.Assert(brect[0] == brect[6], "StringFT: brect[0] != brect[6]"); // left
			Debug.Assert(brect[2] == brect[4], "StringFT: brect[2] != brect[4]"); // right            
			Debug.Assert(brect[5] == brect[7], "StringFT: brect[5] != brect[7]"); // top
			Debug.Assert(brect[1] == brect[3], "StringFT: brect[1] != brect[3]"); // bottom

			bounds = Ntx.GD.Rectangle.FromLTRB(brect[0], brect[5], brect[2], brect[1]);

			return result;
		}


		public string StringFTCircle(int cx, int cy, double radius, double textRadius, double fillPortion, string font, double points, string top, string bottom, GDColor fgcolor)
		{
			string s = GDImport.gdImageStringFTCircle(this.Handle, cx, cy, radius, textRadius, fillPortion, font, points, top, bottom, fgcolor);
			GC.KeepAlive(this);
			return s;
		}
		#endregion

		#region GD Creation/Destruction/Loading/Saving
		/***********************************************************************************\
		GD Creation/Destruction/Loading/Saving
		\***********************************************************************************/

		private GD(IntPtr imageHandle)
		{
			this.handle = imageHandle;

			//this.imageData = (GDImage) Marshal.PtrToStructure( this.Handle, typeof( GDImage ) );

		}

		public static GD Create(int width, int height, bool trueColor)
		{
			IntPtr imageHandle;
			if (trueColor)
				imageHandle = GDImport.gdImageCreateTrueColor(width, height);
			else
				imageHandle = GDImport.gdImageCreate(width, height);

			if (imageHandle == IntPtr.Zero)
				throw new InvalidOperationException("ImageCreate failed.");

			return new GD(imageHandle);
		}

		public static GD Create(ImageType type, string filename)
		{
			if (filename == null)
				throw new ArgumentNullException(nameof(filename));

			IntPtr imageHandle;
			IntPtr fileHandle = IntPtr.Zero;
			try
			{
				fileHandle = GDImport.fopen(filename, "rb");

				if (fileHandle == IntPtr.Zero)
					throw new FileNotFoundException(filename);

				switch (type)
				{
					case ImageType.Jpeg:
						imageHandle = GDImport.gdImageCreateFromJpeg(fileHandle);
						break;
					case ImageType.Png:
						imageHandle = GDImport.gdImageCreateFromPng(fileHandle);
						break;
					case ImageType.Gd:
						imageHandle = GDImport.gdImageCreateFromGd(fileHandle);
						break;
					case ImageType.Gd2:
						imageHandle = GDImport.gdImageCreateFromGd2(fileHandle);
						break;
					case ImageType.WBMP:
						imageHandle = GDImport.gdImageCreateFromWBMP(fileHandle);
						break;
					case ImageType.Xbm:
						imageHandle = GDImport.gdImageCreateFromXbm(fileHandle);
						break;
					case ImageType.Xpm:
						imageHandle = GDImport.gdImageCreateFromXpm(fileHandle);
						break;
					case ImageType.Gif:
						imageHandle = GDImport.gdImageCreateFromGif(fileHandle);
						break;
					default:
						throw new ArgumentOutOfRangeException("type", type + " is unknown import type.");
				}
			}
			finally
			{
				if (fileHandle != IntPtr.Zero) GDImport.fclose(fileHandle);
			}
			if (imageHandle == IntPtr.Zero)
				throw new InvalidOperationException("ImageCreateFrom failed.");

			return new GD(imageHandle);
		}


		public static GD Create(ImageType type, Stream stream)
		{
			IntPtr imageHandle;
			var buffer = new byte[4096];
			int count = 0;
			using (var mem = new MemoryStream())
			{
				while ((count = stream.Read(buffer, 0, buffer.Length)) > 0)
				{
					mem.Write(buffer, 0, count);
				}
				buffer = mem.ToArray();
			}
			imageHandle = IntPtr.Zero;
			switch (type)
			{
				case ImageType.Jpeg:
					imageHandle = GDImport.gdImageCreateFromJpegPtr(buffer.Length, buffer);
					break;
				case ImageType.Png:
					imageHandle = GDImport.gdImageCreateFromPngPtr(buffer.Length, buffer);
					break;
				case ImageType.Gd:
					imageHandle = GDImport.gdImageCreateFromGdPtr(buffer.Length, buffer);
					break;
				case ImageType.Gd2:
					imageHandle = GDImport.gdImageCreateFromGd2Ptr(buffer.Length, buffer);
					break;
				case ImageType.WBMP:
					imageHandle = GDImport.gdImageCreateFromWBMPPtr(buffer.Length, buffer);
					break;
				case ImageType.Xbm:
				//imageHandle = GDImport.gdImageCreateFromXbmPtr(buffer.Length, buffer);
				//break;
				case ImageType.Xpm:
					//imageHandle = GDImport.gdImageCreateFromXpmPtr(buffer.Length, buffer);
					//break;
					throw new NotImplementedException(type + " not implemented.");
				case ImageType.Gif:
					imageHandle = GDImport.gdImageCreateFromGifPtr(buffer.Length, buffer);
					break;
				default:
					throw new ArgumentOutOfRangeException("type", type + " is unknown import type.");
			}

			if (imageHandle == IntPtr.Zero)
				throw new InvalidOperationException("ImageCreateFrom failed.");

			return new GD(imageHandle);
		}


		// Parameter:
		// PNG - level of compression ( 0 - no compression ... )
		// Jpeg - quality
		public bool Save(ImageType type, string filename, int parameter)
		{
			IntPtr fileHandle = GDImport.fopen(filename, "wb");

			if (fileHandle == IntPtr.Zero)
				return false;

			try
			{

				switch (type)
				{
					case ImageType.Jpeg:
						GDImport.gdImageJpeg(this.Handle, fileHandle, parameter);
						break;
					case ImageType.Png:
						GDImport.gdImagePngEx(this.Handle, fileHandle, parameter);
						break;
					case ImageType.Gd:
						GDImport.gdImageGd(this.Handle, fileHandle);
						break;
					case ImageType.Gd2:
						GDImport.gdImageGd2(this.Handle, fileHandle);
						break;
					case ImageType.WBMP:
						GDImport.gdImageWBMP(this.Handle, parameter, fileHandle);
						break;
					case ImageType.Gif:
						GDImport.gdImageGif(this.Handle, fileHandle);
						break;
					case ImageType.Xbm:
					//gdImageXbm( this.Handle, fileHandle );
					//break;
					case ImageType.Xpm:
						//gdImageXpm( this.Handle, fileHandle );
						//break;
						throw new NotImplementedException(type + " not implemented.");
					default:
						throw new ArgumentOutOfRangeException("type", type + " is an unknown file type.");
				}
			}
			finally
			{
				GDImport.fclose(fileHandle);
			}
			GC.KeepAlive(this);
			return true;
		}


		public bool Save(string filename)
		{
			return this.Save(filename, 75);
		}

		public bool Save(string filename, int quality)
		{
			FileInfo fi;
			ImageType ft;
			string ext = "";

			fi = new FileInfo(filename);
			ext = fi.Extension.ToLower();

			switch (ext)
			{
				case ".jpg":
				case ".jpeg":
				case ".jpe":
					ft = ImageType.Jpeg;
					break;
				case ".png":
					ft = ImageType.Png;
					break;
				case ".wbmp":
					ft = ImageType.WBMP;
					break;
				case ".gd":
					ft = ImageType.Gd;
					break;
				case ".gd2":
					ft = ImageType.Gd2;
					break;
				case ".gif":
					ft = ImageType.Gif;
					break;
				default:
					throw new ArgumentOutOfRangeException("type", "Cannot determine file type. pass FileType parameter.");
			}

			return this.Save(ft, filename, quality);
		}

		public bool Save(ImageType type, string filename)
		{
			return this.Save(type, filename, 75);
		}

		public bool Save(Stream outstream)
		{
			//defaulting to jpg, this could be PNG i suppose, but i use jpg more often
			return this.Save(ImageType.Jpeg, outstream, 75);
		}

		public bool Save(ImageType type, Stream outstream)
		{
			return this.Save(type, outstream, 75);
		}

		public bool Save(ImageType type, Stream outstream, int parameter)
		{
			try
			{
				IntPtr imgDataHandle = IntPtr.Zero;
				int len;
				try
				{
					switch (type)
					{
						case ImageType.Jpeg:
							imgDataHandle = GDImport.gdImageJpegPtr(this.Handle, out len, parameter);
							break;
						case ImageType.Png:
							imgDataHandle = GDImport.gdImagePngPtr(this.Handle, out len);
							break;
						case ImageType.Gd:
							imgDataHandle = GDImport.gdImageGdPtr(this.Handle, out len);
							break;
						case ImageType.Gd2:
							imgDataHandle = GDImport.gdImageGd2Ptr(this.Handle, 0, GD2_FMT_COMPRESSED, out len);
							break;
						case ImageType.WBMP:
							imgDataHandle = GDImport.gdImageGdPtr(this.Handle, out len);
							break;
						case ImageType.Xbm:
						case ImageType.Xpm:
							throw new NotImplementedException(type + " not implemented.");
						default:
							throw new ArgumentOutOfRangeException("type", type + " is an unknown file type.");
					}

					if (imgDataHandle == IntPtr.Zero)
						throw new InvalidOperationException("function Save(FileType type, Stream outstream, int parameter )");

					byte[] imgData = new byte[len];

					Marshal.Copy(imgDataHandle, imgData, 0, len);
					MemoryStream img = new MemoryStream(imgData);

					int BUFFER = 4096;
					byte[] buffer = new byte[BUFFER];
					int numBytes;

					while ((numBytes = img.Read(buffer, 0, BUFFER)) > 0)
						outstream.Write(imgData, 0, len);
				}
				finally
				{
					// 2004-01-10 - Must free memory!! - Kevin Tam
					if (imgDataHandle != IntPtr.Zero)
						GDImport.gdFree(imgDataHandle);
				}
			}
			catch (Exception e)
			{
				throw new InvalidOperationException("function Save(FileType type, Stream outstream, int parameter ) failed", e);
			}
			GC.KeepAlive(this);
			return true;
		}

		public void GifAnimBegin(Stream sout)
		{
			GifAnimBegin(sout, true, -1);
		}

		public void GifAnimBegin(Stream sout, bool globalCm)
		{
			GifAnimBegin(sout, globalCm, -1);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sout"></param>
		/// <param name="globalCm"></param>
		/// <param name="loops">Netscape 2/0 extension for animation loop count. 0 - infinite, -1 for not used. -1 is default</param>
		public void GifAnimBegin(Stream sout, bool globalCm, int loops)
		{
			int size;
			using (GDIntPtr rawdata = GDImport.gdImageGifAnimBeginPtr(this.Handle, out size, BoolToInt(globalCm), loops))
			{
				if (rawdata.IsNull)
					throw new InvalidOperationException("Function GifAnimBegin failed");
				byte[] data = new byte[size];
				Marshal.Copy(rawdata, data, 0, size);
				sout.Write(data, 0, size);
			}
			GC.KeepAlive(this);
		}

		public void GifAnimAdd(Stream sout, int localCm, int leftOfs, int topOfs, int delay, Disposal disposal, GD previm)
		{
			int size;
			IntPtr prevhandle = previm == null ? IntPtr.Zero : previm.handle;
			using (GDIntPtr rawdata = GDImport.gdImageGifAnimAddPtr(this.Handle, out size, localCm, leftOfs, topOfs, delay, (int)disposal, prevhandle))
			{
				if (rawdata.IsNull)
					throw new InvalidOperationException("Function GifAnimAdd failed");
				byte[] data = new byte[size];
				Marshal.Copy(rawdata, data, 0, size);
				sout.Write(data, 0, size);
			}
			GC.KeepAlive(this);
		}

		public void GifAnimEnd(Stream sout)
		{
			// Just write a simple semi-colon rather than marshalling a call
			sout.WriteByte((byte)';');
		}

		~GD()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}


		protected virtual void Dispose(bool managed)
		{
			if (!this.disposed)
			{
				GDImport.gdImageDestroy(this.Handle);
				this.disposed = true;
			}
		}


		public void TrueColorToPalette(int ditherFlag, int colorsWanted)
		{
			GDImport.gdImageTrueColorToPalette(this.Handle, ditherFlag, colorsWanted);
			GC.KeepAlive(this);
		}

		public void CheckDisposed()
		{
			if (this.disposed)
				throw new ObjectDisposedException("It has been disposed already.");
		}

		internal IntPtr GetHandle()
		{
			return this.handle;
		}
		#endregion

		#region Queries
		/***********************************************************************************\
		 GD Query
		\***********************************************************************************/

		public void GetClip(ref int x1, ref int y1, ref int x2, ref int y2)
		{
			GDImport.gdImageGetClip(this.Handle, ref x1, ref y1, ref x2, ref y2);
			GC.KeepAlive(this);
		}

		public GDColor GetPixel(int x, int y)
		{
			var color = new GDColor(GDImport.gdImageGetPixel(this.Handle, x, y));
			GC.KeepAlive(this);
			return color;
		}

		public int BoundsSafe(int x, int y)
		{
			int value = GDImport.gdImageBoundsSafe(this.Handle, x, y);
			GC.KeepAlive(this);
			return value;
		}

		public int Width
		{
			get
			{
				return GetStructInt("sx");
			}
		}

		public int Height
		{
			get
			{
				return GetStructInt("sy");
			}
		}

		public int ColorsTotal()
		{
			return GetStructInt("colorsTotal");
		}

		public int Alpha(GDColor color)
		{
			GDImage imageData = (GDImage)Marshal.PtrToStructure(this.Handle, typeof(GDImage));

			if (imageData.trueColor > 0)
				return ((color & 0x7F000000) >> 24);

			GC.KeepAlive(this);
			return imageData.alpha[color];
		}

		public int Red(GDColor color)
		{
			GDImage imageData = (GDImage)Marshal.PtrToStructure(this.Handle, typeof(GDImage));

			if (imageData.trueColor > 0)
				return ((color & 0xFF0000) >> 16);

			GC.KeepAlive(this);
			return imageData.red[color];
		}

		public int Green(GDColor color)
		{
			GDImage imageData = (GDImage)Marshal.PtrToStructure(this.Handle, typeof(GDImage));

			if (imageData.trueColor > 0)
				return ((color & 0x00FF00) >> 8);

			GC.KeepAlive(this);
			return imageData.green[color];
		}

		public int Blue(GDColor color)
		{
			GDImage imageData = (GDImage)Marshal.PtrToStructure(this.Handle, typeof(GDImage));

			if (imageData.trueColor > 0)
				return (color & 0x0000FF);

			GC.KeepAlive(this);
			return imageData.blue[color];
		}

		public bool Interlace
		{
			get
			{
				return IntToBool(GetStructInt("interlace"));
			}
			set
			{
				GDImport.gdImageInterlace(this.Handle, BoolToInt(value));
				GC.KeepAlive(this);
			}
		}

		public GDColor Transparent
		{
			get
			{
				return (GDColor)GetStructInt("transparent");
			}
		}

		public int GetTrueColor()
		{
			return GetStructInt("trueColor");
		}
		#endregion

		#region Copying/Resizing
		/***********************************************************************************\
		GD Copying/Resizing
		\***********************************************************************************/

		public void Copy(GD src, int dstX, int dstY, int srcX, int srcY, int w, int h)
		{
			GDImport.gdImageCopy(this.Handle, src.GetHandle(), dstX, dstY, srcX, srcY, w, h);
			GC.KeepAlive(this);
		}


		public void CopyResized(GD src, int dstX, int dstY, int srcX, int srcY, int destW, int destH, int srcW, int srcH)
		{
			GDImport.gdImageCopyResized(this.Handle, src.GetHandle(), dstX, dstY, srcX, srcY, destW, destH, srcW, srcH);
			GC.KeepAlive(this);
		}

		public void CopyResampled(GD src, int dstX, int dstY, int srcX, int srcY, int destW, int destH, int srcW, int srcH)
		{
			GDImport.gdImageCopyResampled(this.Handle, src.GetHandle(), dstX, dstY, srcX, srcY, destW, destH, srcW, srcH);
			GC.KeepAlive(this);
		}

		public void CopyRotated(GD src, double dstX, double dstY, int srcX, int srcY, int srcW, int srcH, int angle)
		{
			GDImport.gdImageCopyRotated(this.Handle, src.GetHandle(), dstX, dstY, srcX, srcY, srcW, srcH, angle);
			GC.KeepAlive(this);
		}

		public void CopyMerge(GD src, int dstX, int dstY, int srcX, int srcY, int w, int h, int pct)
		{
			GDImport.gdImageCopyMerge(this.Handle, src.GetHandle(), dstX, dstY, srcX, srcY, w, h, pct);
			GC.KeepAlive(this);
		}

		public void PaletteCopy(GD src)
		{
			GDImport.gdImagePaletteCopy(this.Handle, src.GetHandle());
			GC.KeepAlive(this);
		}


		public void SquareToCircle(int radius)
		{
			GDImport.gdImageSquareToCircle(this.Handle, radius);
			GC.KeepAlive(this);
		}

		public void Sharpen(int pct)
		{
			GDImport.gdImageSharpen(this.Handle, pct);
			GC.KeepAlive(this);
		}
		#endregion

		#region Miscellaneous
		/***********************************************************************************\
		GD Miscellaneous
		\***********************************************************************************/

		public void Compare(GD handle2)
		{
			GDImport.gdImageCompare(this.Handle, handle2.Handle);
			GC.KeepAlive(this);
		}

		public void Resize(int destW, int destH)
		{
			CheckDisposed();

			//TODO: resize img and replace this.handle with new img
			IntPtr imageHandle;
			imageHandle = GDImport.gdImageCreateTrueColor(destW, destH);
			if (imageHandle == IntPtr.Zero)
				throw new InvalidOperationException("ImageCreatefailed.");

			GDImport.gdImageCopyResampled(
				imageHandle, this.Handle,
				0, 0, 0, 0,
				destW, destH, this.Width, this.Height);

			GDImport.gdImageDestroy(this.Handle);
			this.handle = imageHandle;
		}
		#endregion

		#region Utility functions

		public IntPtr Handle
		{
			get
			{
				CheckDisposed();
				return this.handle;
			}
		}

		static private int BoolToInt(bool val)
		{
			return val ? (-1) : 0;
		}

		static private bool IntToBool(int val)
		{
			// API uses (-1) for true and 0 for false
			// Will assume that other values are true
			System.Diagnostics.Debug.Assert(val == 0 || val == -1);
			return !(val == 0);
		}

		private int GetStructInt(string field)
		{
			int value = Marshal.ReadInt32(this.Handle, GDImage.GetOffset(field));
			GC.KeepAlive(this);
			return value;
		}
		#endregion
	}

}
