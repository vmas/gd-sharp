﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ntx.GD
{
    public enum ArcType : int
    {
        Arc = 0,
        Pie = 0,
        Chrod = 1,
        NoFill = 2,
        Edged = 4
    }
}
