﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ntx.GD
{
    public enum ImageType : int
    {
        Jpeg = 0,
        Png,
        Gd,
        Gd2,
        WBMP,
        Xbm,
        Xpm,
        Gif
    }
}
