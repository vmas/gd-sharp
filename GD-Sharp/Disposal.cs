﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ntx.GD
{
    public enum Disposal : int
    {
        Unknown = 0,
        None,
        RestoreBackground,
        RestorePrevious
    }
}
