﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Ntx.GD
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Rectangle
    {
        private int left;
        private int top;
        private int right;
        private int bottom;

        public int X
        {
            get
            {
                return left;
            }
            set
            {
                int width = right - left;
                left = value;
                right = value + width;
            }
        }

        public int Y
        {
            get { return top; }
            set {
                int height = bottom - top;
                top = value;
                bottom = value + height;
            }
        }

        public int Width
        {
            get { return right - left; }
            set { right = left + value; }
        }

        public int Height
        {
            get { return bottom - top; }
            set { bottom = top + value; }
        }

        public int Right
        {
            get
            {
                return right;
            }
        }

        public int Bottom
        {
            get { return bottom; }
        }

        public Rectangle(int x, int y, int width, int height)
        {
            left = x;
            top = y;
            right = x + width;
            bottom = y + height;
        }

        public static Rectangle FromLTRB(int left, int top, int right, int bottom)
        {
            var rect = new Rectangle();
            rect.left = left;
            rect.top = top;
            rect.right = right;
            rect.bottom = bottom;
            return rect;
        }

        public bool IntersectsWith(Rectangle rect)
        {
            return rect.left < this.right && this.left < rect.right && rect.top < this.bottom && this.top < rect.bottom;
        }

        public bool IntersectsWithAny(IEnumerable<Rectangle> rects)
        {
            foreach(Rectangle r in rects)
            {
                if (this.IntersectsWith(r)) return true;
            }
            return false;
        }

        public static Rectangle Union(Rectangle a, Rectangle b)
        {
            var rect = new Rectangle();
            rect.left = Math.Min(a.left, b.left);
            rect.top = Math.Min(a.top, b.top);
            rect.right = Math.Max(a.right, b.right);
            rect.bottom = Math.Max(a.bottom, b.bottom);
            return rect;
        }

 

 


    }
}
